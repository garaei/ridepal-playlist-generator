import pool from './pool.js';
import data from './base-data.js';

const searchBy = async (query) => {
  const sql = `
    SELECT 
      u.id,
      u.username,
      u.avatar,
      u.created_on,
      u.role_id,
      (SELECT count(*) FROM users u
      WHERE u.is_deleted = 0) as total
    FROM users u
    WHERE u.is_deleted = 0 ${data.searchBy(query)}
    `;

  const result = await pool.query(sql);

  return {
    total: result[0] ? result[0].total : 0,
    data: result,
  };
};

const getAuth = async (username) => {
  const sql = `
    SELECT 
      u.id, 
      u.username,
      u.password,
      u.role_id,
      r.role,
      u.is_deleted
    FROM users u
    JOIN roles r ON u.role_id = r.id
    WHERE u.username = ?;
    `;

  return (await pool.query(sql, [username]))[0];
};

const getBy = async (column, value) => {
  const sql = `
    SELECT 
      u.id,
      u.username,
      u.avatar,
      u.created_on,
      u.role_id
    FROM users u
    WHERE u.${column} = ?
    AND u.is_deleted = 0;
    `;

  return (await pool.query(sql, [value]))[0];
};

const create = async ({ username, email, password }) => {
  const sql = `
    INSERT INTO users (username, email, password)
    VALUES (?, ?, ?);
    `;

  const result = await pool.query(sql, [username, email, password]);

  return {
    id: result.insertId,
    username,
  };
};

const update = async (column, value, id) => {
  const sql = `
    UPDATE users SET
    ${column} = ?
    WHERE id = ?;
    `;

  return await pool.query(sql, [value, id]);
};

const remove = async (id) => {
  const sql = `
    UPDATE users
    SET is_deleted = 1
    WHERE id = ?
    `;
  return await pool.query(sql, [id]);
};

export default {
  getAuth,
  getBy,
  searchBy,
  create,
  update,
  remove,
};
