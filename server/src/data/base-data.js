const createSortQuery = (sortBy) => {
  if (!sortBy) return '';
  if (Array.isArray(sortBy)) return `ORDER BY ${sortBy.map(el => `${el.split('.')[0]} ${el.split('.')[1].toUpperCase()}`).join(', ')}`;
  return `ORDER BY ${sortBy.split('.')[0]} ${sortBy.split('.')[1].toUpperCase()}`;
};

const createLimitQuery = (limit) => {
  if (!limit || Number.isNaN(limit)) return '';
  if (+limit < 0) limit = 2;
  return `LIMIT ${limit}`;
};

const createOffsetQuery = (offset) => {
  if (!offset || Number.isNaN(offset)) return '';
  if ((+offset) < 0) offset = 0;
  return `OFFSET ${offset}`;
};

const createFilterQuery = (filters) => {
  if (!filters) return '';
  return Object.keys(filters).map(key => {
    if (Array.isArray(filters[key])) {
      return `AND (${filters[key].map(element => `${key} LIKE '%${element}%'`).join(' OR ')})`;
    }
    return `AND ${key} LIKE '%${filters[key]}%'`;
  }).join(' ');
};

const parseQueryParams = (query = {}) => {
  const { sortBy, limit, offset, rating, genre_id, ...filters } = query;

  return {
    sortBy: createSortQuery(sortBy),
    limit: createLimitQuery(limit),
    offset: createOffsetQuery(offset),
    filters: createFilterQuery(filters),
  };
};

const searchBy = (query) => {
  const { limit, offset, sortBy, filters } = parseQueryParams(query);

  const sql = `
    ${filters}
    ${sortBy}
    ${limit}
    ${offset}`;
  return sql;
};

export default {
  searchBy,
};
