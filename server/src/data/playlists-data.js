import pool from './pool.js';

const getAll = async (query) => {
  const sql = `
 SELECT *
 FROM playlists;`;
  const result = await pool.query(sql);
  return {
    data: result,
  };
};

export default {
  getAll,
};
