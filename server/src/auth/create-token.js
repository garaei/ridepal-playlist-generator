import jwt from 'jsonwebtoken';
import config from '../config.js';

const { auth } = config;

const createToken = (payload) => {
  const token = jwt.sign(
    payload,
    auth.PRIVATE_KEY,
    { expiresIn: auth.TOKEN_LIFETIME },
  );

  return token;
};

export default createToken;
