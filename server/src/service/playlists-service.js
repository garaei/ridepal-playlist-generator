const getAllPlaylists = playlistsData => async (query) => await playlistsData.getAll(query);

export default {
  getAllPlaylists,
};
