import express from 'express';
import asyncHandler from 'express-async-handler';

import usersData from '../data/users-data.js';

import usersService from '../service/users-service.js';
import serviceErrors from '../common/service-errors.js';

import validateBody from '../middlewares/validate-body.js';
import authUser from '../middlewares/auth-user.js';
import transformBody from '../middlewares/transform-body.js';
import fileUpload from '../middlewares/file-upload.js';
import tokenLogoutUsers from '../middlewares/token-logout-users.js';
import limitQuery from '../middlewares/limit-query.js';

import createUserValidator from '../validators/create-user-validator.js';
import updateUserValidator from '../validators/update-user-validator.js';

const usersRouter = express.Router();

usersRouter.get('/',
  limitQuery,
  asyncHandler(async (req, res) => {
    res
      .status(200)
      .json(await usersService.getAllUsers(usersData)(req.query));
  }));

usersRouter.get('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  asyncHandler(async (req, res) => {
    const { id } = req.params;

    const result = await usersService.getUserById(usersData)(id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `User with id ${id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

usersRouter.post('/',
  transformBody(createUserValidator),
  validateBody('user', createUserValidator),
  asyncHandler(async (req, res) => {
    const result = await usersService.createUser(usersData)(req.body);

    if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Username or email already exist.' });
    } else {
      res
        .status(201)
        .json(result.data);
    }
  }));

usersRouter.put('/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  transformBody(updateUserValidator),
  validateBody('user', updateUserValidator),
  asyncHandler(async (req, res) => {
    const result = await usersService.updateUser(usersData)(+req.params.id, req.body, req.user);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this user.' });
    } else if (result.error === serviceErrors.DUPLICATE_RECORD) {
      res
        .status(409)
        .json({ message: 'Username or email already exist.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

usersRouter.put('/avatars',
  authUser,
  asyncHandler(tokenLogoutUsers),
  fileUpload,
  asyncHandler(async (req, res) => {
    const result = await usersService.addAvatar(usersData)(+req.params.id, req.file, req.user);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      res
        .status(403)
        .json({ message: 'Not allowed to update this user.' });
    } else if (result.error === serviceErrors.INVALID_REQUEST) {
      res
        .status(409)
        .json({ message: 'Please add an image in .png, .jpg and .jpeg format to upload.' });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

export default usersRouter;
