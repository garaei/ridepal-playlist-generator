import express from 'express';
import playlistsService from '../service/playlists-service.js';
import playlistsData from '../data/playlists-data.js';

const playlistsRouter = express.Router();

playlistsRouter.get('/', async (req, res) => {
  const result = await playlistsService.getAllPlaylists(playlistsData)(req.query);

  res
    .status(200)
    .json(result);
});

export default playlistsRouter;
