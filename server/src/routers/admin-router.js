import express from 'express';
import asyncHandler from 'express-async-handler';

import usersData from '../data/users-data.js';

import authUser from '../middlewares/auth-user.js';
import validateUserRole from '../middlewares/validate-role.js';
import tokenLogoutUsers from '../middlewares/token-logout-users.js';

import usersService from '../service/users-service.js';
import serviceErrors from '../common/service-errors.js';

const adminRouter = express.Router();

adminRouter.get('/users',
  authUser,
  asyncHandler(tokenLogoutUsers),
  validateUserRole('admin'),
  asyncHandler(async (req, res) => {
    res
      .status(200)
      .json(await usersService.getAllUsers(usersData)(req.query));
  }));

adminRouter.delete('/users/:id',
  authUser,
  asyncHandler(tokenLogoutUsers),
  validateUserRole('admin'),
  asyncHandler(async (req, res) => {
    const result = await usersService.removeUser(usersData)(+req.params.id);

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      res
        .status(404)
        .json({ message: `User with id ${+req.params.id} not found.` });
    } else {
      res
        .status(200)
        .json(result.data);
    }
  }));

export default adminRouter;
