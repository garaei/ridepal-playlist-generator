import express from 'express';
import asyncHandler from 'express-async-handler';

import createToken from '../auth/create-token.js';

import tokensData from '../data/tokens-data.js';
import usersData from '../data/users-data.js';

import usersService from '../service/users-service.js';
import serviceErrors from '../common/service-errors.js';

import tokenLogoutUsers from '../middlewares/token-logout-users.js';
import authUser from '../middlewares/auth-user.js';

const authRouter = express.Router();

/**
 * @swagger
 * /v1/login:
 *   post:
 *     description: Login
 *     tags:
 *     - Login
 *     consumes:
 *     - application/json
 *     produces:
 *     - application/json
 *     parameters:
 *     - in: body
 *       name: body
 *       required: true
 *       schema:
 *         type: object
 *         properties:
 *           username:
 *             type: string
 *           password:
 *             type: string
 *     responses:
 *       200:
 *         description: Returns a JWT.
 *         schema:
 *           type: object
 *           properties:
 *             token:
 *               type: string
 *       400:
 *         description: Record not found.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *       401:
 *         description: Invalid credentials.
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 */
authRouter.post('/login',
  asyncHandler(async (req, res) => {
    const { username, password } = req.body;
    const result = await usersService.signInUser(usersData)(username, password);

    if (result.error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res
        .status(401)
        .json({ message: 'Invalid user or password.' });
    }

    if (result.error === serviceErrors.RECORD_NOT_FOUND) {
      return res
        .status(400)
        .json({ message: 'User has been deleted.' });
    }

    const user = result.data;

    const payload = {
      id: user.id,
      username: user.username,
      role_id: user.role_id,
      role: user.role,
    };

    const token = createToken(payload);

    res
      .status(200)
      .json({ token });
  }));

authRouter.post('/logout', authUser,
  asyncHandler(tokenLogoutUsers),
  asyncHandler(async (req, res) => {
    await tokensData.blacklistToken(req.headers.authorization.replace('Bearer ', ''));

    res.json({ message: 'Successfully logged out!' });
  }));

export default authRouter;
