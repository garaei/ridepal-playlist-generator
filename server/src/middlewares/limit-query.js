export default async (req, res, next) => {
  const { limit } = req.query;

  if (!limit || limit > 100) {
    req.query.limit = 100;
  }

  await next();
};
