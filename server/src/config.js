import dotenv from 'dotenv';

const env = dotenv.config().parsed;

const auth = {
  PRIVATE_KEY: 'bwBdYr6UG2nhguIN9Gs2',
  TOKEN_LIFETIME: 240 * 60,
};

const swaggerOptions = {
  definition: {
    swagger: '2.0',
    info: {
      title: 'RidePal – Playlist Generator',
      version: '1.0.0',
    },
    servers: [
      {
        url: 'http://localhost:5555/v1',
        description: 'Development server',
      },
    ],
  },
  apis: ['src/routers/*.js'], // files containing annotations as above
};

export default {
  env,
  auth,
  swaggerOptions,
};
