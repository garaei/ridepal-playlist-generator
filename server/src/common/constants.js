const VALID_MIMETYPES = ['image/png', 'image/jpeg'];

export default {
  VALID_MIMETYPES,
};
