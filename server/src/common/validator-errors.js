export default {
  user: {
    username: 'Expected name to be string with length [8-20]. '
    + 'Could include alphanumeric characters, underscore and dot. '
    + 'Underscores and dots could not be at the beginning or end of the username, and can not be consecutive.',
    password: 'Expected password to be string with length [8-20]. '
    + 'Should contain at least one number, one lowercase letter, one capital letter and one special symbol.',
    email: 'Expected email to be valid email with length of at least 7 symbols.',
  },
};
