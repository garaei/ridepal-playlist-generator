/* eslint-disable no-useless-escape */

const PLAYLIST_FORM_CONTROLS = {};

const REGISTER_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g,
    },
    valid: false,
    touched: false,
  },
  email: {
    name: 'email',
    placeholder: 'Email',
    value: '',
    validation: {
      required: true,
      minLength: 7,
      regex: /^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g,
    },
    valid: false,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      required: true,
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: false,
    touched: false,
  },
};

const LOGIN_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      required: true,
    },
    valid: false,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      required: true,
    },
    valid: false,
    touched: false,
  },
};

const USER_FORM_CONTROLS = {
  username: {
    name: 'username',
    placeholder: 'Username',
    value: '',
    validation: {
      regex: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/g,
    },
    valid: true,
    touched: false,
  },
  email: {
    name: 'email',
    placeholder: 'Email',
    value: '',
    validation: {
      minLength: 7,
      regex: /^(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-z]+$/g,
    },
    valid: true,
    touched: false,
  },
  password: {
    name: 'password',
    placeholder: 'Password',
    value: '',
    validation: {
      regex: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[\]:;<>,.?\/~_+\-=|\\]).{8,20}$/g,
    },
    valid: true,
    touched: false,
  },
};

export default {
  PLAYLIST_FORM_CONTROLS,
  REGISTER_FORM_CONTROLS,
  LOGIN_FORM_CONTROLS,
  USER_FORM_CONTROLS,
};
