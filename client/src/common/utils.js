const isInputValid = (input, validations, files) => {
  let isValid = true;

  if (validations.required) {
    isValid = isValid && input.length !== 0;
  }

  if (validations.minLength) {
    isValid = isValid && input.length >= validations.minLength;
  }

  if (validations.maxLength) {
    isValid = isValid && input.length <= validations.maxLength;
  }

  if (validations.validFileTypes) {
    isValid = isValid && validations.validFileTypes.includes(files[0].type);
  }

  if (validations.regex) {
    isValid = isValid && input.match(validations.regex);
  }

  if (!validations.required) {
    isValid = isValid || input === '';
  }

  return isValid;
};

const handleInputChange = (event, form, setForm, setIsFormValid) => {
  const { name, value, files } = event.target;
  const updatedControl = { ...form[name] };

  updatedControl.value = value;

  if (files) {
    updatedControl.files = files;
    updatedControl.preview = URL.createObjectURL(files[0]);
  }

  updatedControl.touched = true;
  if (!value && !files) updatedControl.touched = false;

  updatedControl.valid = isInputValid(value, updatedControl.validation, files);

  const updatedForm = { ...form, [name]: updatedControl };
  setForm(updatedForm);

  const formValid = Object.values(updatedForm).every(
    (control) => control.valid
  );

  setIsFormValid(formValid);
};

export default {
  handleInputChange,
};
