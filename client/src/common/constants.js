const BING_KEY = 'AmqtWU4SK30MQyYDt3xKRrnuQ7HNyn_vxGe2q0ZJORuNVvf1KkwdFZdcRP51Zp8x';
const BING_API = 'http://dev.virtualearth.net/REST/v1/Locations';
const BING_API_DISTANCE = 'https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix';

const requestOptions = {
  method: 'GET',
  redirect: 'manual'
};

export default {
  BING_KEY,
  BING_API,
  BING_API_DISTANCE,
  requestOptions,
};
