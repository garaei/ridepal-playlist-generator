import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { StylesProvider } from '@material-ui/core/styles';
import { useContext } from 'react';
import { AuthContext } from 'providers/АuthContext';
import Box from '@material-ui/core/Box';
import Home from 'pages/Home/Home';
import PlaylistsList from 'pages/PlaylistsList/PlaylistsList';
import MakePlaylist from 'pages/MakePlaylist/MakePlaylist';
import NoMatch from 'pages/NoMatch/NoMatch';
import Navigation from 'components/Navigation/Navigation';
import Login from 'pages/Login/Login';
import Register from 'pages/Register/SignUp';
import UserProfile from 'pages/UserProfile/UserProfile';
import GuardedRoute from 'providers/GuardedRoute';
import AdminUsersList from 'pages/Admin/Users/AdminUsersList';
import AdminPlaylistsList from 'pages/Admin/Playlists/AdminPlaylistsList';
import ServerError from 'components/ServerError/ServerError';

const App = () => {
  const { user } = useContext(AuthContext);

  return (

    <StylesProvider injectFirst>
      <CssBaseline />
      <Router>
        <Navigation />
        <Box component='main'>
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route exact path="/home" component={Home} />
            <Route exact user={user} path="/login" component={Login} />
            <Route exact user={user} path="/register" component={Register} />

            <GuardedRoute exact user={user} path="/playlists" component={PlaylistsList} />

            <Route exact user={user} path="/makeplaylist" component={MakePlaylist} />

            <GuardedRoute exact user={user} admin path="/admin/users" component={AdminUsersList} />
            <GuardedRoute exact user={user} admin path="/admin/playlists" component={AdminPlaylistsList} />

            <GuardedRoute exact user={user} path="/profile" component={UserProfile} />

            <Route path="/500" component={ServerError} />
            <Route path="*" component={NoMatch} />
          </Switch>
        </Box>
      </Router>
    </StylesProvider>
  );
};

export default App;
