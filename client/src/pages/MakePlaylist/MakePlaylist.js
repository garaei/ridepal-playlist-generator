import React, { useState } from 'react';
import constants from 'common/constants.js';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TripStartEndPoint from 'pages/MakePlaylist/TripStartEndPoint';
import RepeatArtist from 'pages/MakePlaylist/RepeatArtist';
import Review from 'pages/MakePlaylist/ReviewPlaylist';
import ActionAlert from 'components/ActionAlert/ActionAlert';

const useStyles = makeStyles((theme) => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const steps = ['Enter start and end point of trip', 'Repeat artist?', 'Review your playlist'];

function getStepContent(step, setStartPoint,
  setEndPoint, duration, setIntialStateStart, setIntialStateEnd) {
  switch (step) {
    case 0:
      return (
        <TripStartEndPoint
          setStartPoint={setStartPoint}
          setEndPoint={setEndPoint}
          setIntialStateStart={setIntialStateStart}
          setIntialStateEnd={setIntialStateEnd}
        />
      );
    case 1:
      return <RepeatArtist duration={duration} />;
    case 2:
      return <Review />;
    default:
      throw new Error('Unknown step');
  }
}

const MakePlaylist = () => {
  const history = useHistory();
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);

  const [notification, setNotification] = useState({ isOpen: false, type: '', message: '' });
  const handleNotificationClose = () => setNotification({ isOpen: false, type: '', message: '' });

  const [initialStateStart, setIntialStateStart] = useState(false);
  const [initialStateEnd, setIntialStateEnd] = useState(false);

  const [startPoint, setStartPoint] = useState();
  const [endPoint, setEndPoint] = useState();
  const [duration, setDuration] = useState(0);

  const handleNext = () => {
    if (activeStep === 0) {
      if (initialStateStart && initialStateEnd) {
        fetch(`${constants.BING_API_DISTANCE}?origins=${startPoint.join(',')}&destinations=${endPoint.join(',')}&travelMode=driving&key=${constants.BING_KEY}`, constants.requestOptions)
          .then(response => response.json())
          .then(result => {
            setDuration(result.resourceSets[0].resources[0].results[0].travelDuration);
          })

          .catch(() => history.push('/500'));
        setActiveStep(activeStep + 1);
      } else {
        setNotification({
          isOpen: true,
          type: 'error',
          message: 'Enter valid endpoints',
        });
      }
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <Paper className={classes.paper}>
      <Typography component="h1" variant="h4" align="center">
        Checkout
      </Typography>
      <Stepper activeStep={activeStep} className={classes.stepper}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <>
        {activeStep === steps.length ? (
          <>
            <Typography variant="h5" gutterBottom>
              Rock your trip
            </Typography>
            <Typography variant="subtitle1">
              show option to listen current playlist...
            </Typography>
          </>
        ) : (
          <>
            {getStepContent(activeStep, setStartPoint,
              setEndPoint, duration, setIntialStateStart, setIntialStateEnd)}
            <div className={classes.buttons}>
              {activeStep !== 0 && (
              <Button onClick={handleBack} className={classes.button}>
                Back
              </Button>
              )}
              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? 'Save playlist' : 'Next'}
              </Button>
            </div>
          </>
        )}
      </>
      <ActionAlert
        notification={notification}
        handleClose={handleNotificationClose}
      />
    </Paper>
  );
};

export default MakePlaylist;
