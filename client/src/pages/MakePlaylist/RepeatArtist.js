import React from 'react';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const RepeatArtist = ({ duration }) => (
  <Typography variant="h6" gutterBottom>
    Handle duration of
    {' '}
    {duration}
    {' '}
    minutes!
  </Typography>

);

RepeatArtist.propTypes = {
  duration: PropTypes.number.isRequired,

};

export default RepeatArtist;
