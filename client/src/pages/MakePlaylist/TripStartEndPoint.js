/* eslint-disable object-curly-newline */
import React, { useEffect, useState } from 'react';
import constants from 'common/constants.js';
// import { useHistory } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PropTypes from 'prop-types';

const TripStartEndPoint = ({ setStartPoint,
  setEndPoint, setIntialStateStart, setIntialStateEnd }) => {
  // const history = useHistory();

  const [startAddress, setStartAddress] = useState('');
  const [endAddress, setEndAddress] = useState('');

  const [startSearch, setStartSearch] = useState([]);
  const [endSearch, setEndSearch] = useState([]);

  const handleStartAddress = (event) => {
    setEndSearch([]);
    setStartAddress(event.target.value);
  };

  const handleEndAddress = (event) => {
    setStartSearch([]);
    setEndAddress(event.target.value);
  };
  const handleClickStartPoint = (e) => {
    setStartAddress(e.target.innerHTML);
  };

  const handleClickEndPoint = (e) => {
    setEndAddress(e.target.innerHTML);
  };

  const hideSearchStartPoint = () => {
    setStartSearch([]);
  };

  const hideSearchEndPoint = () => {
    setEndSearch([]);
  };

  useEffect(() => {
    fetch(`${constants.BING_API}?query=${startAddress}&key=${constants.BING_KEY}`, constants.requestOptions)
      .then(response => response.json())
      .then(result => {
        setStartSearch(result.resourceSets[0].resources
          .map(location => location.address.formattedAddress));
        setStartPoint(result.resourceSets[0].resources[0].point.coordinates);
        setIntialStateStart(true);
      })

      .catch(() => setIntialStateStart(false));
  }, [startAddress]);

  useEffect(() => {
    fetch(`${constants.BING_API}?query=${endAddress}&key=${constants.BING_KEY}`, constants.requestOptions)
      .then(response => response.json())
      .then(result => {
        setEndSearch(result.resourceSets[0].resources
          .map(location => location.address.formattedAddress));
        setEndPoint(result.resourceSets[0].resources[0].point.coordinates);
        setIntialStateEnd(true);
      })
      .catch(() => setIntialStateEnd(false));
  }, [endAddress]);

  return (
    <>
      <Typography variant="h6" gutterBottom>
        Choose your trip
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <TextField
            required
            id="startPoint"
            name="startPoint"
            label="Start point"
            fullWidth
            autoComplete="given-name"
            value={startAddress}
            onChange={(e) => handleStartAddress(e)}
            onClick={hideSearchEndPoint}
          />
        </Grid>
        {startSearch && (
          startSearch.map((searchResult) => (
            <Grid item xs={12} direction='row' onClick={(e) => handleClickStartPoint(e)}>
              <LocationOnIcon />
              <Typography variant="h6" gutterBottom>
                {searchResult}
              </Typography>
            </Grid>
          )))}

        <Grid item xs={12}>
          <TextField
            required
            id="endPoint"
            name="endPoint"
            label="End point"
            fullWidth
            autoComplete="family-name"
            value={endAddress}
            onChange={(e) => handleEndAddress(e)}
            onClick={hideSearchStartPoint}
          />
        </Grid>
        {endSearch && (
          endSearch.map((searchResult) => (
            <Grid item xs={12} direction='row' onClick={(e) => handleClickEndPoint(e)}>
              <LocationOnIcon />
              <Typography variant="h6" gutterBottom>
                {searchResult}
              </Typography>
            </Grid>
          )))}
      </Grid>
    </>
  );
};

TripStartEndPoint.propTypes = {
  setStartPoint: PropTypes.func.isRequired,
  setEndPoint: PropTypes.func.isRequired,
  setIntialStateStart: PropTypes.bool.isRequired,
  setIntialStateEnd: PropTypes.bool.isRequired,
};
export default TripStartEndPoint;
