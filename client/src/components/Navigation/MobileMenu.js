import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { AuthContext } from 'providers/АuthContext';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },

}));
const MobileMenu = ({
  mobileMoreAnchorEl, isMobileMenuOpen, handleMenuClose, logoutHandler
}) => {
  const mobileMenuId = 'primary-search-account-menu-mobile';
  const classes = useStyles();
  const auth = useContext(AuthContext);
  return (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      handleMenuClose
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMenuClose}
    >
      {!auth.user ? (
        <div>
          <MenuItem
            className={classes.navButton}
            component={NavLink}
            to="/login"
            onClick={handleMenuClose}
          >
            Login
          </MenuItem>
          <MenuItem
            className={classes.navButton}
            component={NavLink}
            to="/register"
            onClick={handleMenuClose}
          >
            Register
          </MenuItem>
        </div>
      ) : (
        <div>
          <MenuItem
            className={classes.navButton}
            component={NavLink}
            to="/home"
            onClick={logoutHandler}
          >
            Logout
          </MenuItem>
          <MenuItem
            className={classes.navButton}
            component={NavLink}
            to="/profile"
            onClick={handleMenuClose}
          >
            My Profile
          </MenuItem>
        </div>
      )}
    </Menu>
  );
};

MobileMenu.propTypes = {
  mobileMoreAnchorEl: PropTypes.bool.isRequired,
  isMobileMenuOpen: PropTypes.bool.isRequired,
  logoutHandler: PropTypes.func.isRequired,
  handleMenuClose: PropTypes.func.isRequired,
};

export default MobileMenu;
