import React, { useState, useContext, useEffect } from 'react';
import { useHistory, NavLink } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { AuthContext } from 'providers/АuthContext';
import authService from 'services/auth';
import userService from 'services/user';
import AdminMenu from 'components/Navigation/AdminMenu';
import MobileMenu from 'components/Navigation/MobileMenu';
import MenuNav from 'components/Navigation/MenuNav';

const useStyles = makeStyles((theme) => ({
  itemContainer: {
    width: 'auto',
  },
  navButton: {
    marginRight: theme.spacing(0.5),
    marginLeft: theme.spacing(0.5),
  },
  title: {
    marginRight: 16,
  },
  navLink: {
    color: theme.palette.secondary.light,
    textDecoration: 'none',
    '&:hover': {
      color: theme.palette.secondary.light,
    },
    fontSize: theme.spacing(2),
  },
  navLinkActive: {
    color: theme.palette.secondary.dark,
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  }
}));

const Navigation = () => {
  const classes = useStyles();
  const auth = useContext(AuthContext);
  const history = useHistory();

  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [adminAnchorEl, setAdminAnchorEl] = useState(null);

  const theme = useTheme();
  const isSmall = useMediaQuery(theme.breakpoints.down('sm'));
  const isXSmall = useMediaQuery(theme.breakpoints.down('xs'));

  const isMenuNavOpen = Boolean(anchorEl);
  const isAdminNavOpen = Boolean(adminAnchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  useEffect(() => {
    if (auth.user) {
      userService.getUser(+auth.user.id)
        .then((result) => {
          auth.setAvatar(result.avatar);
        })
        .catch(() => history.push('/500'));
    }
  }, [auth]);

  const logoutHandler = () => {
    setMobileMoreAnchorEl(null);
    authService.logout()
      .then(auth.setUser(null))
      .then(localStorage.removeItem('token'))
      .then(localStorage.removeItem('avatar'))
      .then(history.push('/home'))
      .catch(() => history.push('/500'));
  };

  const handleMenuNavOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setAdminAnchorEl(null);
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleAdminOpen = (event) => {
    event.preventDefault();
    setAdminAnchorEl(event.currentTarget);
  };
  const adminMenuId = 'primary-search-account-admin-menu';
  const menuId = 'primary-search-account-menu';
  const mobileMenuId = 'primary-search-account-menu-mobile';
  return (
    <>
      <AppBar position="static">
        <Toolbar component={Grid} container spacing={2} wrap='nowrap' justify='space-between'>
          <Grid item hidden={!isSmall}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleMenuNavOpen}
            >
              <MenuIcon />
            </IconButton>
            <MenuNav
              handleMenuClose={handleMenuClose}
              anchorEl={anchorEl}
              menuId={menuId}
              isMenuNavOpen={isMenuNavOpen}
            />
          </Grid>

          <Grid item hidden={isSmall}>
            <Grid container spacing={1} className={classes.itemContainer} alignItems='center'>
              <Grid item>
                <Typography className={classes.title} variant="h6" noWrap>
                  We will rock you!
                </Typography>
              </Grid>
              <Grid item>
                <NavLink to='/home' className={classes.navLink} activeClassName={classes.navLinkActive}> Home </NavLink>
              </Grid>
              <Grid item />
              <Grid item>
                <NavLink to='/playlists' className={classes.navLink} activeClassName={classes.navLinkActive}> Playlists </NavLink>
              </Grid>
              <Grid item />
              <Grid item>
                <NavLink to='/makeplaylist' className={classes.navLink} activeClassName={classes.navLinkActive}> Make Playlist </NavLink>
              </Grid>
              <Grid item />
              {auth.user && auth.user.role === 'admin' && (
                <>
                  <Grid item>
                    <NavLink to='#' className={classes.navLink} activeClassName={classes.navLinkActive} isActive={(match, location) => location.pathname.includes('admin')} onClick={handleAdminOpen}> Admin </NavLink>
                    <AdminMenu
                      adminAnchorEl={adminAnchorEl}
                      handleMenuClose={handleMenuClose}
                      isAdminNavOpen={isAdminNavOpen}
                      adminMenuId={adminMenuId}
                    />
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>
          <Grid item hidden={!isXSmall}>
            <IconButton
              edge='end'
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
            <MobileMenu
              mobileMoreAnchorEl={mobileMoreAnchorEl}
              isMobileMenuOpen={isMobileMenuOpen}
              logoutHandler={logoutHandler}
              handleMenuClose={handleMenuClose}
            />
          </Grid>
          <Grid item hidden={isXSmall}>
            <Grid container spacing={1} className={classes.itemContainer} alignItems='center'>
              <Grid item>
                {auth.user ? (<NavLink to='/home' className={classes.navLink} activeClassName={classes.navLinkActive} onClick={logoutHandler}> Logout </NavLink>)
                  : (<NavLink to='/login' className={classes.navLink} activeClassName={classes.navLinkActive}> Login </NavLink>)}
              </Grid>
              <Grid item />
              <Grid item>
                {auth.user ? (
                  <NavLink to='/profile' className={classes.navLink} activeClassName={classes.navLinkActive}>
                    <Avatar alt={auth.user.username} src={`/images/${auth.avatar}`} className={classes.avatar} />
                  </NavLink>
                )
                  : (<NavLink to='/register' className={classes.navLink} activeClassName={classes.navLinkActive}> Register </NavLink>)}
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

    </>
  );
};

// Navigation.propTypes = {
//   history: PropTypes.isRequired,
//   push: PropTypes.isRequired,

// };
export default Navigation;
