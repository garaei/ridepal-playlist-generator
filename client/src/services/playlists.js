import { fetchData, ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const getAllPlaylists = (params) => {
  const query = params ? `${ENDPOINTS.PLAYLISTS}?${querystring.stringify(params)}` : `${ENDPOINTS.PLAYLISTS}`;

  return fetchData(query, 'GET', undefined, true);
};

const getPlaylist = (id) => fetchData(`${ENDPOINTS.PLAYLISTS}/${id}`, 'GET', undefined, true);

export default {
  getAllPlaylists, getPlaylist
};
