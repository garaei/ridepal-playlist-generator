import { fetchData, uploadData, ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const register = (username, email, password) => fetchData(ENDPOINTS.USERS, 'POST', { username, email, password }, false);

const getUser = (id) => fetchData(`${ENDPOINTS.USERS}/${id}`, 'GET', undefined, true);

const getAllUsers = (params) => {
  const query = params ? `${ENDPOINTS.USERS}?${querystring.stringify(params)}` : `${ENDPOINTS.USERS}`;

  return fetchData(query, 'GET', undefined, true);
};

const uploadAvatar = (id, formData) => uploadData(`${ENDPOINTS.USERS}/${id}/avatars`, 'PUT', formData);

const updateProfile = (id, body) => fetchData(`${ENDPOINTS.USERS}/${id}`, 'PUT', body, true);

const getUserHistory = (id) => fetchData(`${ENDPOINTS.USERS}/${id}/history`, 'GET', undefined, true);

const getUserReviews = (id) => fetchData(`${ENDPOINTS.USERS}/${id}/reviews`, 'GET', undefined, true);

export default {
  register, getUser, uploadAvatar, updateProfile, getAllUsers, getUserHistory, getUserReviews
};
