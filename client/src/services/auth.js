import { fetchData, ENDPOINTS } from 'services/serviceBase';

const login = (username, password) => fetchData(ENDPOINTS.LOGIN, 'POST', { username, password }, false);

const logout = () => fetchData(ENDPOINTS.LOGOUT, 'POST', undefined, true);

export default { login, logout };
