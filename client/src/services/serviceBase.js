export const API_VERSION = '/v1';

export const ENDPOINTS = {
  PLAYLISTS: `${API_VERSION}/playlists`,
  USERS: `${API_VERSION}/users`,
  ADMIN: `${API_VERSION}/admin`,
  LOGIN: `${API_VERSION}/login`,
  LOGOUT: `${API_VERSION}/logout`,
  GENRES: `${API_VERSION}/genres`,
};

export const fetchData = (endpoint, method, body, isAuth = true) => fetch(endpoint, {
  method,
  headers: {
    'Content-Type': 'application/json',
    Authorization: isAuth ? `Bearer ${localStorage.getItem('token')}` : undefined,
  },
  body: JSON.stringify(body),
})
  .then(r => r.json());

export const uploadData = (endpoint, method, formData) => fetch(endpoint, {
  method,
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  },
  body: formData,
})
  .then(r => r.json());
