import { fetchData, ENDPOINTS } from 'services/serviceBase';

const getAllGenres = () => fetchData(`${ENDPOINTS.GENRES}`, 'GET', undefined, true);

export default { getAllGenres };
