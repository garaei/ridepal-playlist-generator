import { fetchData, uploadData, ENDPOINTS } from 'services/serviceBase';
import querystring from 'querystring';

const getAllPlaylists = (params) => {
  const query = params ? `${ENDPOINTS.ADMIN}/playlists?${querystring.stringify(params)}` : `${ENDPOINTS.ADMIN}/playlists`;

  return fetchData(query, 'GET', undefined, true);
};

const getAllUsers = (params) => {
  const query = params ? `${ENDPOINTS.ADMIN}/users?${querystring.stringify(params)}` : `${ENDPOINTS.ADMIN}/users`;

  return fetchData(query, 'GET', undefined, true);
};

const deleteUser = (id) => fetchData(`${ENDPOINTS.ADMIN}/users/${id}`, 'DELETE', undefined, true);

const deletePlaylist = (id) => fetchData(`${ENDPOINTS.ADMIN}/playlists/${id}`, 'DELETE', undefined, true);

const toggleBanStatus = (id) => fetchData(`${ENDPOINTS.ADMIN}/users/${id}`, 'PATCH', undefined, true);

const createPlaylist = (formData) => uploadData(`${ENDPOINTS.ADMIN}/playlists`, 'POST', formData);

const updatePlaylist = (id, formData) => uploadData(`${ENDPOINTS.ADMIN}/playlists/${id}`, 'PUT', formData);

export default {
  getAllPlaylists,
  getAllUsers,
  deleteUser,
  deletePlaylist,
  toggleBanStatus,
  createPlaylist,
  updatePlaylist
};
