import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const GuardedRoute = ({
  component: Component, user, admin, ...rest
}) => {
  const renderComponent = (props) => {
    if (user) {
      if (!admin || (admin && user.role === 'admin')) {
        return <Component {...props} />;
      }

      return <Redirect to='/home' />;
    }

    return <Redirect to='/login' />;
  };

  return (
    <Route
      {...rest}
      render={renderComponent}
    />
  );
};

GuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    role_id: PropTypes.number.isRequired,
    role: PropTypes.string.isRequired,
  }),
  admin: PropTypes.bool,
};

GuardedRoute.defaultProps = {
  user: null,
  admin: false,
};

export default GuardedRoute;
